
# Configure CI Pipeline Webhook Trigger
In this project we will configure webhooks to trigger our CI Pipeline.

## Technologies Used
- Jenkins
- GitLab
- Git
- Docker
- Java
- Maven

## Project Description
- Install GitLab Plugins in Jenkins
- Configure GitLab access token and connection to Jenkins in GitLab project settings
- Configure Jenkins to trigger the CI Pipeline, whenever a change is pushed to GitLab

## Prerequisites
- Jenkins installed on a remote server as a Docker container
- Docker Hub account
- Gitlab account and a repository
- Private Repository with the **java-maven-app** code in it. [Gitlab Link](https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/java-maven-app)

## Guide Steps | Pipeline Job
### Installing GitLab Jenkins Plugin
- Manage Jenkins > Plugins > Available Plugins
- Search for **GitLab**
- Select **GitLab** and **Install without restart**
	- Note: if you do a restart your Docker container for Jenkins may need to be restarted
- Manage Jenkins > System > GitLab section
	- Connection Name: **gitlab-conn**
	- Gitlab host URL: https://gitlab.com/
		- **Note**:  If you use another service or host it yourself this will change. This is not a repository URL, only the repositories host URL
	- Credentials: **Newly created from below steps**
		- Add > Jenkins
		- Domain: **Global credentials (unrestricted)**
		- Kind: **GitLab API Token**
		- Scope: **Global**
		- API Token: **Configure GitLab Access Token Section**
		- ID: **gitlab-token**
		- **Add**
	- Verify it works by pressing **Test Connection**, a **Success** message should pop-up if everything is configured correctly!
	- **Save**

![Jenkins Configured Webhook](/images/m8-4-jenkins-configured-webhook.png)

### Configure GitLab Access Token
- Log into your GitLab account
- Account > Preferences > Access Tokens > **Add new token**
	- Token Name: **jenkins**
	- Expiration date: **Anytime in the future**
	- Scope: **api**
	- **Create personal access token**
- **Note**: If you need to save it, click reveal and save it somewhere safe. If you don't do it now you will never be able to see it again and you will need to create a new token.

![GitLab Access Token Created](/images/m8-4-gitlab-access-token-creation.png)

### Enabling Configurations in a Pipeline Job
- Navigate to a pipeline job > Configure
	- Under General you will see a **GitLab Connection** option with our configured **gitlab-conn**
	- You can also toggle on **Build when a change is pushed...** under **Build Triggers** to allow for builds to happen automatically for specific job

![GitLab Connection Added](/images/m8-4-gitlab-connection.png)

![Build Triggers Added](/images/m8-4-build-triggers.png)

### Configure Jenkins Webhook Trigger
- From your GitLab project, navigate to Settings > Integrations > Jenkins > **Configure**
	- Enable Integration: **Active**
	- Trigger: **Push**
	- Jenkins Server URL: **http://JENKINS_IP:PORT**
	- SSL Verification: **Off**
	- Project Name: **my-pipeline**
		- This is the name of the job inside Jenkins, mine is named **my-pipeline**
	- Username: chris
		- Your Jenkins username
	- **Save Changes** 

![GitLab Success](/images/m8-4-successful-gitlab-connection.png)

![Jenkins Success](/images/m8-4-successful-jenkins-connection.png)


## Guide Steps | Multi-Branch Pipeline
### Installing Multibranch Scan Jenkins Plugin
- Manage Jenkins > Plugins > Available plugins
- Search for **multibranch scan webhook trigger**
- Install without restart

### Configure Jenkins Project
- Dashboard > Multibranch Pipeline Job > Configure
- Scroll down to **Scan Multibranch Pipeline Triggers**
	- Enable **Scan by webook**
	- Trigger Token: **gitlabtoken**
		- Anything you want to be used to authenticate the Repo <> Jenkins connection
		- Clicking the **?** for the Trigger token will get you the URL that we will need in the **Configure GitLab** section
	- **Save**

![Configured Jenkins Project](/images/m8-4-configured-multibranch-webhook-scan.png)

### Configure GitLab
- Project > Settings > Webhooks > **Add new webhook**
- URL: **JENKINS_URL:PORT/multibranch-webhook-trigger/invoke?token=gitlabtoken**
- Trigger: **Push**
	- **All Branches**
- **Add Webhook**

![Multibranch Webhook Settings in Gitlab](/images/m8-4-multibranch-webhook-gitlab-settings.png)